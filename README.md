# Psycho Production Libraries #
This library contains at the moment 2 utility packages.  This library is presented under the BSD licence.

* Spinner This package is a little tool to produce a spinning cursor for the console.
* Cache This package is a general cache.

Each package has its own unit tests for reference and to prove they do what they are supposed to do.
Everything is fully documented in the code, with full method descriptions.

## Spinner ##

The spinner has 2 methods Start and Stop, which do exactly what they seem to do.
The Start method will start the spinning cursor at the current position on the console, then display the next element in the animation every x milliseconds.  As long as the console is static the cursor will spin in place.  Calling Stop will, well stop it.


```
#!c#
var spinner = new Spinner();

spinner.Start(200);
System.Console.Read();
spinner.Stop();

```

The Spinner will run in its own thread, and not interrupt anything being processed by the rest of the application.

## Cache ##

The Cache package is designed to store whatever is needed for up to a given amount of time.
The Cache can store objects in either a global pool (global to the cache object) or in a dedicated pool of a given object type.
Items added to the cache are given a default of infinity (well until the end of the year 3000) to live.  Once the time to live on an object has been reached it will be cleared out of the cache the next time it is read.  Adding an object to a cache (global or type cache) with a key that is already in use will cause the existing item to be ejected and replaced with the new one.


```
#!c#

var testCache = new Cache();
var key = "test";
var data = "bob";

testCache.AddItem(key, data);

var value = testCache.LookupValue(key);

testCache.DeleteItem(key) ? Console.Write("item deleted") : Console.Write("item not found");

```

There are unit tests that cover most of the cache code.  The area's that are lacking involve concurrency.
The Cache will lock when trying to add or delete items to prevent race conditions.