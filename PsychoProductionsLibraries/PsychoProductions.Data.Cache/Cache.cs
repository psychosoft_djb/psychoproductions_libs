//-----------------------------------------------------------------------
// <copyright file="Cache.cs" company="Psycho Productions">
//      <author>David Buckley</author>
//      <email>david@psychosoft.co.uk</email>
// </copyright>
//-----------------------------------------------------------------------
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2013, Psycho Productions
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace PsychoProductions.Data.Cache
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Caching class.
    /// Stores items either in one global pool of data,
    /// or in a pool for each type of object requesting a cache.
    /// Using a pool per object type allows for key collisions between object types.
    /// If the IsLocked is true then no updates to the cache can happen.
    /// This prevents race conditions if multiple threads are using the same cache.
    /// </summary>
    public class Cache : IDisposable
    {
        /// <summary>
        /// The lock when in the add method
        /// </summary>
        private static readonly object Addlock = new object();

        /// <summary>
        /// The lock when in the delete method
        /// </summary>
        private readonly object deletelock = new object();

        /// <summary>
        /// The type used for a cache made for anything
        /// </summary>
        private readonly Type globalcache;

        /// <summary>
        /// The caches.
        /// A dictionary based on type.
        /// Each dictionary item is a cache for items from a given type of object
        /// </summary>
        private Dictionary<Type, Dictionary<string, CacheItem>> caches;

        /// <summary>
        /// Has this been disposed.
        /// </summary>
        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="PsychoProductions.Data.Cache.Cache"/> class.
        /// </summary>
        public Cache()
        {
            this.globalcache = typeof(CacheItem);
            this.caches = new Dictionary<Type, Dictionary<string, CacheItem>>();
            this.disposed = false;
            this.AddCache(this.globalcache);
        }

        /// <summary>
        /// Adds an item to the cache for a given source object type
        /// </summary>
        /// <param name="key">The key for this cache item</param>
        /// <param name="value">The value to store</param>
        /// <param name="ttl">Time To Live.  Number of seconds this item is valid for.  Negative numbers have no time out.</param>
        /// <param name="cachetype">The type of object this is being cached for.  If null then use the global cache.</param>
        /// <returns><c>true</c>, if item was added, <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">The value of '<see cref="F:PsychoProductions.Data.Cache.CacheItem.key" /> cannot be null. </exception>
        public bool AddItem(string key, object value, int ttl, Type cachetype)
        {
            var added = false;

            if (!string.IsNullOrWhiteSpace(key))
            {
                var item = CacheItem.MakeCacheItem(key, value, ttl);
                added = this.AddItem(item, cachetype);
            }

            return added;
        }

        /// <summary>
        /// Adds an item to the cache for a given source object type
        /// </summary>
        /// <param name="item">The item to add to the cache.</param>
        /// <param name="cachetype">The type of object this is being cached for.  If null then use the global cache.</param>
        /// <returns><c>true</c>, if item was added, <c>false</c> otherwise.</returns>
        public bool AddItem(CacheItem item, Type cachetype)
        {
            var added = false;

            if (item != null)
            {
                added = this.LockAndAdd(item, cachetype);
            }

            return added;
        }
        
        /// <summary>
        /// Adds an item to the cache for a given source object type.
        /// Items have no expiration.
        /// </summary>
        /// <param name="key">The key for this cache item</param>
        /// <param name="value">The value to store</param>
        /// <param name="cachetype">The type of object this is being cached for.  If null then use the global cache.</param>
        /// <returns><c>true</c>, if item was added, <c>false</c> otherwise.</returns>
        public bool AddItem(string key, object value, Type cachetype)
        {
            return this.AddItem(key, value, -1, cachetype);
        }

        /// <summary>
        /// Adds an item to the cache.
        /// Items have no expiration.
        /// </summary>
        /// <param name="key">The key for this cache item</param>
        /// <param name="value">The value to store</param>
        /// <param name="ttl">Time To Live.  Number of seconds this item is valid for.  Negative numbers have no time out.</param>
        /// <returns><c>true</c>, if item was added, <c>false</c> otherwise.</returns>
        public bool AddItem(string key, object value, int ttl = -1)
        {
            return this.AddItem(key, value, ttl, this.globalcache);
        }

        /// <summary>
        /// Deletes the item from a given cache.
        /// </summary>
        /// <param name="key">The key of the value to delete</param>
        /// <param name="cachetype">The type of cache object to get.  If null then use the global cache.</param>
        /// <returns><c>true</c>, if item was deleted, <c>false</c> otherwise.</returns>
        public bool DeleteItem(string key, Type cachetype)
        {
            var deleted = false;
            var cache = this.GetCache(cachetype);

            lock (this.deletelock)
            {
                if (cache != null)
                {
                    if (cache.ContainsKey(key))
                    {
                        deleted = cache.Remove(key);
                    }
                }
            }

            return deleted;
        }

        /// <summary>
        /// Deletes an item from the global pool
        /// </summary>
        /// <param name="key">The key of the value to delete</param>
        /// <returns><c>true</c>, if item was deleted, <c>false</c> otherwise.</returns>
        public bool DeleteItem(string key)
        {
            return this.DeleteItem(key, this.globalcache);
        }

        /// <summary>
        /// Releases all resource used by the <see cref="PsychoProductions.Data.Cache.Cache"/> object.
        /// </summary>
        /// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="PsychoProductions.Data.Cache.Cache"/>.
        /// The <see cref="Dispose"/> method leaves the <see cref="PsychoProductions.Data.Cache.Cache"/> in an unusable
        /// state. After calling <see cref="Dispose"/>, you must release all references to the
        /// <see cref="PsychoProductions.Data.Cache.Cache"/> so the garbage collector can reclaim the memory that the
        /// <see cref="PsychoProductions.Data.Cache.Cache"/> was occupying.</remarks>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// Lookups the value of a key from a given cache.
        /// </summary>
        /// <param name="key">The key of the value to lookup</param>
        /// <param name="cachetype">The cache to look in.  If null then use the global cache.</param>
        /// <returns>The value if found or null if not or it has expired</returns>
        public object LookupValue(string key, Type cachetype)
        {
            var cache = this.GetCache(cachetype);
            var item = GetItemFromCache(key, cache);

            if (item != null)
            {
                if (item.IsExpired)
                {
                    this.DeleteItem(key, cachetype);
                }
            }

            return GetItemData(item);
        }

        /// <summary>
        /// Lookups the value of a key from the global cache.
        /// </summary>
        /// <param name="key">The key of the value to lookup</param>
        /// <returns>The value if found or null if not or it has expired</returns>
        public object LookupValue(string key)
        {
            return this.LookupValue(key, this.globalcache);
        }

        /// <summary>
        /// Gets the item data.
        /// </summary>
        /// <param name="item">The item to get data from</param>
        /// <returns>The item data or null if it has expired</returns>
        private static object GetItemData(CacheItem item)
        {
            object value = null;

            if (item != null)
            {
                if (!item.IsExpired)
                {
                    value = item.Value;
                }
            }

            return value;
        }

        /// <summary>
        /// Gets the item from cache.
        /// </summary>
        /// <param name="key">The key to look for in the cache.</param>
        /// <param name="cache">The cache to look in.</param>
        /// <returns>The item from cache, or null if it doesn't exist</returns>
        private static CacheItem GetItemFromCache(string key, IDictionary<string, CacheItem> cache)
        {
            CacheItem item = null;

            if ((cache != null) && (!string.IsNullOrWhiteSpace(key)))
            {
                if (cache.ContainsKey(key))
                {
                    cache.TryGetValue(key, out item);
                }
            }

            return item;
        }

        /// <summary>
        /// Adds a new type of cache to the caches.
        /// </summary>
        /// <param name="cachetype">The type of cache to add.  If null then use the global cache.</param>
        /// <returns><c>true</c>, if cache was added, <c>false</c> otherwise.</returns>
        private bool AddCache(Type cachetype)
        {
            var added = false;
            var ctype = this.CacheType(cachetype);

            if (!this.caches.ContainsKey(ctype))
            {
                this.caches.Add(ctype, new Dictionary<string, CacheItem>());
                added = this.caches.ContainsKey(ctype);
            }

            return added;
        }

        /// <summary>
        /// Get the correct cache.
        /// </summary>
        /// <returns>The type of cache to get</returns>
        /// <param name="requested">The cache to get.  If null then use the global cache.</param>
        private Type CacheType(Type requested)
        {
            return requested ?? this.globalcache;
        }

        /// <summary>
        /// Dispose this object.
        /// </summary>
        /// <param name="disposing">If set to <c>true</c> disposing.</param>
        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.caches.Clear();
                    this.caches = null;

                    this.disposed = true;
                }
            }
        }

        /// <summary>
        /// Gets the cache for a given type
        /// </summary>
        /// <param name="cachetype">The type of cache to get.  If null then use the global cache.</param>
        /// /// <returns>The cache for the requested type or null if doesn't exist</returns>
        private Dictionary<string, CacheItem> GetCache(Type cachetype)
        {
            Dictionary<string, CacheItem> cache = null;
            var ctype = this.CacheType(cachetype);

            if (this.caches.ContainsKey(ctype))
            {
                this.caches.TryGetValue(ctype, out cache);
            }

            return cache;
        }

        /// <summary>
        /// Get the cache or make a new one if its doesn't exist for the given type.
        /// </summary>
        /// <param name="ctype">The type of cache to get.</param>
        /// <returns>The cache for the requested type or null.</returns>
        private Dictionary<string, CacheItem> GetOrMakeNewCache(Type ctype)
        {
            var cache = this.GetCache(ctype);

            if (cache == null)
            {
                if (this.AddCache(ctype))
                {
                    cache = this.GetCache(ctype);
                }
            }

            return cache;
        }

        /// <summary>
        /// Lock the cache and add an item.
        /// </summary>
        /// <param name="item">The item to add to the cache.</param>
        /// <param name="cachetype">The type of object this is being cached for.  If null then use the global cache.</param>
        /// <returns><c>true</c>, if item was added, <c>false</c> otherwise.</returns>
        private bool LockAndAdd(CacheItem item, Type cachetype)
        {
            var added = false;

            if (item != null)
            {
                lock (Addlock)
                {
                    var ctype = this.CacheType(cachetype);
                    this.DeleteItem(item.Key, cachetype);
                    var cache = this.GetOrMakeNewCache(ctype);

                    // we need a cache and it should only have been locked once here or we can't do anything
                    if (cache != null)
                    {
                        cache.Add(item.Key, item);

                        added = true;
                    }
                }
            }

            return added;
        }
    }
}