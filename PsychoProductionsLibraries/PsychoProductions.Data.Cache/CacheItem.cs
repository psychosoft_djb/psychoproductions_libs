//-----------------------------------------------------------------------
// <copyright file="CacheItem.cs" company="Psycho Productions">
//      <author>David Buckley</author>
//      <email>david@psychosoft.co.uk</email>
// </copyright>
//-----------------------------------------------------------------------
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2013, Psycho Productions
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace PsychoProductions.Data.Cache
{
    using System;

    /// <summary>
    /// Cache item.
    /// Holds the data for an item in the cache.
    /// </summary>
    public class CacheItem : IDisposable
    {
        /// <summary>
        /// The date to use for the end date on a never expiring cache item
        /// </summary>
        private const string Infinity = "3000-12-31";

        /// <summary>
        /// The error message when the key is empty or null
        /// </summary>
        private const string Nokeymessage = "No key value supplied";

        /// <summary>
        /// The date and time that this cache item expires.
        /// </summary>
        private readonly DateTime expires;

        /// <summary>
        /// has the dispose been run
        /// </summary>
        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="PsychoProductions.Data.Cache.CacheItem"/> class.
        /// </summary>
        /// <param name="key">The key to identify this item</param>
        /// <param name="value">The value that this is holding</param>
        /// <param name="ttl">Time To Live in seconds, negative is 3000-12-31</param>
        /// <exception cref="ArgumentNullException">The value of '<see cref="Key"/> cannot be null. </exception>
        public CacheItem(string key, object value, int ttl)
        {
            const string Keyname = "key";

            this.disposed = false;

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentNullException(Keyname, Nokeymessage);
            }

            this.Key = key;
            this.Value = value;

            if (ttl < 0)
            {
                DateTime.TryParse(Infinity, out this.expires);
            }
            else
            {
                this.expires = DateTime.Now + GetTimeSpan(ttl);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is expired.
        /// </summary>
        /// <value><c>true</c> if this instance is expired; otherwise, <c>false</c>.</value>
        public bool IsExpired
        {
            get
            {
                return DateTime.Compare(DateTime.Now, this.expires) > 0;
            }
        }

        /// <summary>
        /// Gets the key.
        /// </summary>
        /// <value>The key.</value>
        public string Key { get; private set; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>The value.</value>
        public object Value { get; private set; }

        /// <summary>
        /// Make a new CacheItem.
        /// </summary>
        /// <param name="key">The key to identify this item</param>
        /// <param name="value">The value that this is holding</param>
        /// <param name="ttl">Time To Live in seconds, default is until the year 3000.</param>
        /// <returns>A new <see cref="CacheItem"/>.</returns>
        /// <exception cref="ArgumentNullException">The value of '<see cref="F:PsychoProductions.Data.Cache.CacheItem.key" /> cannot be null. </exception>
        public static CacheItem MakeCacheItem(string key, object value, int ttl = -1)
        {
            return new CacheItem(key, value, ttl);
        }

        /// <summary>
        /// Releases all resource used by the <see cref="PsychoProductions.Data.Cache.CacheItem"/> object.
        /// </summary>
        /// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="PsychoProductions.Data.Cache.CacheItem"/>.
        /// The <see cref="Dispose"/> method leaves the <see cref="PsychoProductions.Data.Cache.CacheItem"/> in an unusable
        /// state. After calling <see cref="Dispose"/>, you must release all references to the
        /// <see cref="PsychoProductions.Data.Cache.CacheItem"/> so the garbage collector can reclaim the memory that the
        /// <see cref="PsychoProductions.Data.Cache.CacheItem"/> was occupying.</remarks>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// Get a new time span object for the given seconds
        /// </summary>
        /// <param name="seconds">The number of seconds to get the time span for</param>
        /// <returns>The time span for the given seconds</returns>
        private static TimeSpan GetTimeSpan(int seconds)
        {
            var hour = seconds / 3600;
            var remainder = seconds - (hour * 3600);
            var min = remainder / 60;
            var sec = remainder - (min * 60);

            return new TimeSpan(hour, min, sec);
        }

        /// <summary>
        /// Dispose the specified disposing.
        /// </summary>
        /// <param name="disposing">If set to <c>true</c> disposing.</param>
        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.Value = null;
                    this.Key = null;
                    this.disposed = true;
                }
            }
        }
    }
}