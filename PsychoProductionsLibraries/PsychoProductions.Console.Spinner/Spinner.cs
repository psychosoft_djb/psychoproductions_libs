//-----------------------------------------------------------------------
// <copyright file="Spinner.cs" company="Psycho Productions">
//      <author>David Buckley</author>
//      <email>david@psychosoft.co.uk</email>
// </copyright>
//-----------------------------------------------------------------------
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2014, Psycho Productions
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace PsychoProductions.Console.Spinner
{
    using System;
    using System.Threading;

  /// <summary>
  /// This class provides a spinning cursor for a console application
  /// </summary>
  public class Spinner
  {
    /// <summary>
    /// the format string to use when displaying the spinner
    /// </summary>
    private const string DisplayFormat = "\b{0}";

    /// <summary>
    /// The stages of the spinner
    /// </summary>
    private readonly string[] stages = 
    { 
        @"|", 
        @"/", 
        @"-", 
        @"|", 
        @"\",
        @"-" 
    };

    /// <summary>
    /// The thread running the spinner
    /// </summary>
    private Thread displaythread;

    /// <summary>
    /// Is the spinner running or not
    /// </summary>
    private bool running;

    /// <summary>
    /// Initializes a new instance of the <see cref="PsychoProductions.Console.Spinner.Spinner"/> class.
    /// </summary>
    public Spinner()
    {
      this.running = false;
    }

      /// <summary>
      /// Start the spinner with a given delay between stages
      /// </summary>
      /// <param name="milliseconds">the delay between stages</param>
      /// <exception cref="ThreadStateException">The thread has already been started. </exception>
      /// <exception cref="OutOfMemoryException">There is not enough memory available to start this thread. </exception>
      public void Start(int milliseconds)
    {
      ThreadStart runthis = () => this.Display(milliseconds);

      if (milliseconds > 0)
      {
        this.displaythread = new Thread(runthis);
        this.running = true;
        this.displaythread.Start();
      }
    }

    /// <summary>
    /// Stop this instance.
    /// </summary>
    public void Stop()
    {
      this.running = false;
      this.displaythread = null;
    }

    /// <summary>
    /// Display the spinner on the console while <see cref="running"/>
    /// is true.
    /// </summary>
    /// <param name="milliseconds">The number of milliseconds to wait before updating the spinner.</param>
    private void Display(int milliseconds)
    {
      var position = 0;

      while (this.running)
      {
        if (position >= this.stages.Length)
        {
          position = 0;
        }

        Console.Write(DisplayFormat, this.stages[position]);
        position++;
        Thread.Sleep(milliseconds);
      }
    }
  }
}
