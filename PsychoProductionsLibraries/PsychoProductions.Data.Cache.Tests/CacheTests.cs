//-----------------------------------------------------------------------
// <copyright file="CacheTests.cs" company="Psycho Productions">
//      <author>David Buckley</author>
//      <email>david@psychosoft.co.uk</email>
// </copyright>
//-----------------------------------------------------------------------
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2013, Psycho Productions
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace PsychoProductions.Data.Cache.Tests
{
  using System;
  using System.Globalization;
  using System.Threading;
  using NUnit.Framework;
  using PsychoProductions.Data.Cache;

  /// <summary>
  /// Tests for the cache code
  /// </summary>
  [TestFixture]
  public class CacheTests
  {
    /// <summary>
    /// Test the constructor
    /// </summary>
    [Test]
    public void CacheConstructorTest()
    {
      Assert.IsNotNull(new Cache(), "Cache object was created as null");
    }

    /// <summary>
    /// Test for adding an item using all the parameters
    /// </summary>
    [Test]
    public void AddItemAllParamsThreadedTest()
    {
      const string Value = "test";
      var testCache = new Cache();
      var valueType = Value.GetType();
      var addedInThread1 = false;
      var addedInThread2 = false;
      var thread1 = new Thread(() => addedInThread1 = testCache.AddItem("test1", "tester", -1, valueType));
      var thread2 = new Thread(() => addedInThread2 = testCache.AddItem("test1", "testing", -1, valueType));

      StartAndWaitForThreadsToFinish(thread1, thread2);

      var r2 = testCache.LookupValue("test1", valueType);
      Assert.IsNotNull(r2, "nothing was returned when test1 was looked up");

      // if a1 != a2 then thats fine
      // if they are the same then both adds worked but only one of the values should be there
      if ((addedInThread1 == addedInThread2) && addedInThread1)
      {
        Assert.IsNotNull(r2, "Nothing was saved but the return says something was");

        var first = string.CompareOrdinal(r2.ToString(), "tester");
        var second = string.CompareOrdinal(r2.ToString(), "testing");

        Assert.IsTrue(first == 0 || second == 0, "neither returns where what was expected");
      }
    }

    /// <summary>
    /// Test for adding an item using all the parameters
    /// </summary>
    [Test]
    public void AddItemAllParamsTest()
    {
      const int Value1 = 1;
      const string Value2 = "test";
      var testCache = new Cache();
      var value1Type = Value1.GetType();
      var value2Type = Value2.GetType();

      // test to see if we can add 2 items with a clash of keys but different types
      Assert.IsTrue(testCache.AddItem("test", Value1, -1, value1Type), "Added valid data and was told it failed");
      Assert.IsTrue(testCache.AddItem("test", Value2, -1, value2Type), "Added valid data and was told it failed");

      var result1 = testCache.LookupValue("test", value1Type);
      var result2 = testCache.LookupValue("test", value2Type);

      Assert.IsTrue(string.CompareOrdinal(result1.ToString(), Value1.ToString(CultureInfo.InvariantCulture)) == 0, string.Format("Comparison of [{0}] and [{1}] determined they where not the same and should have been", Value1, result1));
      Assert.IsTrue(string.CompareOrdinal(result2.ToString(), Value2) == 0, string.Format("Comparison of [{0}] and [{1}] determined they where not the same and should have been", Value2, result2));
    }

    /// <summary>
    /// Test for adding an item using all the parameters
    /// </summary>
    [Test]
    public void AddItemAllParamsThreadedTtlTest()
    {
      var testCache = new Cache();
      const string Value = "test";
      var valueType = Value.GetType();

      // test that the add works when threaded and the first add's value has no time to live
      var addedInThread1 = false;
      var addedInThread2 = false;
      var thread1 = new Thread(() => addedInThread1 = testCache.AddItem("test1", "test3", -1, valueType));
      var thread2 = new Thread(() => addedInThread2 = testCache.AddItem("test1", "test4", -1, valueType));

      StartAndWaitForThreadsToFinish(thread1, thread2, 1010);

      var result = testCache.LookupValue("test1", valueType);
      Assert.IsNotNull(result, "nothing was returned when test1 was looked up");
      Assert.IsTrue(addedInThread1 == addedInThread2, "The results of the thread adding are not the same and should be"); 
      Assert.IsTrue(string.CompareOrdinal(result.ToString(), "test4") == 0, string.Format("The expected value of [test4] was not returned got [{0}]", result));
    }

    /// <summary>
    /// Test for adding an item using all the parameters as null
    /// </summary>
    [Test]
    public void AddItemAllParamsNullTest()
    {
      var testCache = new Cache();
      const int Value = 1;
      var valueType = Value.GetType();

      Assert.IsFalse(testCache.AddItem(null, null, 0, null), "Tried to add nulls as an item and was told it worked");
      Assert.IsFalse(testCache.AddItem(string.Empty, null, 0, null), "Tried to add empty sting key as an item and was told it worked");

      Assert.IsFalse(testCache.AddItem(null, Value, 0, valueType), "Tried to add nulls as a k and was told it worked");
      Assert.IsFalse(testCache.AddItem(string.Empty, Value, 0, valueType), "Tried to add empty sting key as an item and was told it worked");
    }

    /// <summary>
    /// Test for adding then overwriting an item using all the parameters
    /// </summary>
    [Test]
    public void AddItemAllParamsOverwriteTest()
    {
      var testCache = new Cache();
      var value = 1;
      var cachetype = value.GetType();
      
      Assert.IsTrue(testCache.AddItem("test", value, -1, cachetype), "Added valid data and was told it failed");
      var result1 = testCache.LookupValue("test", cachetype);
      Assert.IsTrue(string.CompareOrdinal(result1.ToString(), value.ToString(CultureInfo.InvariantCulture)) == 0, string.Format("Comparison of [{0}] and [{1}] determined they where not the same and should have been", value, result1));

      // overwrite test
      value = 100;
      Assert.IsTrue(testCache.AddItem("test", value, -1, cachetype), "Added valid data and was told it failed");
      var result2 = testCache.LookupValue("test", cachetype);
      Assert.IsTrue(string.CompareOrdinal(result2.ToString(), value.ToString(CultureInfo.InvariantCulture)) == 0, string.Format("Comparison of [{0}] and [{1}] determined they where not the same and should have been", value, result2));
      Assert.IsFalse(string.CompareOrdinal(result1.ToString(), result2.ToString()) == 0, string.Format("Comparison of [{0}] and [{1}] determined they where the same and should have been", result1, result2));

      // force adding to default cache
      Assert.IsTrue(testCache.AddItem("test", value, 0, null), "Added valid data and was told it failed");
    }

    /// <summary>
    /// Test for adding an item using all items except time to live
    /// Items have no expiration.
    /// </summary>
    [Test]
    public void AddItemKeyValueTypeTest()
    {
      var testCache = new Cache();
      var value = 1;
      var valueType = value.GetType();

      Assert.IsTrue(testCache.AddItem("test", value, valueType), "Added valid data and was told it failed");
      var result1 = testCache.LookupValue("test", valueType);
      Assert.IsTrue(string.CompareOrdinal(result1.ToString(), value.ToString(CultureInfo.InvariantCulture)) == 0, string.Format("Comparison of [{0}] and [{1}] determined they where not the same and should have been", value, result1));

      // overwrite test
      value = 100;
      Assert.IsTrue(testCache.AddItem("test", value, valueType), "Added valid data and was told it failed");
      var result2 = testCache.LookupValue("test", valueType);
      Assert.IsTrue(string.CompareOrdinal(result2.ToString(), value.ToString(CultureInfo.InvariantCulture)) == 0, string.Format("Comparison of [{0}] and [{1}] determined they where not the same and should have been", value, result2));
      Assert.IsFalse(string.CompareOrdinal(result1.ToString(), result2.ToString()) == 0, string.Format("Comparison of [{0}] and [{1}] determined they where the same and should have been", result1, result2));

      // force adding to default cache
      Assert.IsTrue(testCache.AddItem("test", value, null), "Added valid data and was told it failed");
    }

    /// <summary>
    /// Test for adding an item using all items except time to live
    /// Items have no expiration.
    /// </summary>
    [Test]
    public void AddItemKeyValueTypeThreadedTest()
    {
      var testCache = new Cache();
      const string Value = "test";
      var valueType = Value.GetType();
      var addedInThread1 = false;
      var addedInThread2 = false;
      var thread1 = new Thread(() => addedInThread1 = testCache.AddItem("test1", "tester", valueType));
      var thread2 = new Thread(() => addedInThread2 = testCache.AddItem("test1", "testing", valueType));

      StartAndWaitForThreadsToFinish(thread1, thread2);

      var result = testCache.LookupValue("test1", valueType);
      Assert.IsNotNull(result, "nothing was returned when test1 was looked up");

      // if a1 != a2 then that's fine
      // if they are the same then both adds worked but only one of the values should be there
      if ((addedInThread1 == addedInThread2) && addedInThread1)
      {
        Assert.IsNotNull(result, "Nothing was saved but the return says something was");

        var first = string.Compare(result.ToString(), "tester", StringComparison.Ordinal);
        var second = string.Compare(result.ToString(), "testing", StringComparison.Ordinal);

        Assert.IsTrue(first == 0 || second == 0, "neither returns where what was expected");
      }
    }

    /// <summary>
    /// Test for adding a null items using the global cache
    /// Items have no expiration.
    /// </summary>
    [Test]
    public void AddItemKeyValueTtlNullTest()
    {
      var testCache = new Cache();
      const int Value = 1;

      Assert.IsFalse(testCache.AddItem(null, null, 0), "Tried to add nulls as an item and was told it worked");
      Assert.IsFalse(testCache.AddItem(string.Empty, null, 0), "Tried to add empty sting key as an item and was told it worked");

      Assert.IsFalse(testCache.AddItem(null, Value, 0), "Tried to add nulls as a k and was told it worked");
      Assert.IsFalse(testCache.AddItem(string.Empty, Value, 0), "Tried to add empty sting key as an item and was told it worked");
    }

    /// <summary>
    /// Test for adding an item using the global cache using the min and max values for an integer as the TTL.
    /// Items have no expiration.
    /// </summary>
    [Test]
    public void AddItemKeyValueTtlMinMaxTest()
    {
      var testCache = new Cache();
      const int Value = 1;
      const string Key = "test";

      Assert.IsTrue(testCache.AddItem(Key, Value, int.MaxValue), string.Format("Added valid data with ttl of [{0}] and was told it failed", int.MaxValue));
      Assert.IsTrue(testCache.AddItem(Key, Value, int.MinValue), string.Format("Added valid data with ttl of [{0}] and was told it failed", int.MinValue));
    }

    /// <summary>
    /// Test for adding an item using the global cache
    /// Items have no expiration.
    /// </summary>
    [Test]
    public void AddItemKeyValueTtlTest()
    {
      var testCache = new Cache();
      var value = 1;

      // overwrite test
      Assert.IsTrue(testCache.AddItem("test", value), "Added valid data and was told it failed");
      var result1 = testCache.LookupValue("test");
      Assert.IsTrue(string.CompareOrdinal(result1.ToString(), value.ToString(CultureInfo.InvariantCulture)) == 0, string.Format("Comparison of [{0}] and [{1}] determined they where not the same and should have been", value, result1));

      value = 100;
      Assert.IsTrue(testCache.AddItem("test", value), "Added valid data and was told it failed");
      var result2 = testCache.LookupValue("test");
      Assert.IsTrue(string.CompareOrdinal(result2.ToString(), value.ToString(CultureInfo.InvariantCulture)) == 0, string.Format("Comparison of [{0}] and [{1}] determined they where not the same and should have been", value, result2));
      Assert.IsFalse(string.CompareOrdinal(result1.ToString(), result2.ToString()) == 0, string.Format("Comparison of [{0}] and [{1}] determined they where the same and should have been", result1, result2));

      Assert.IsTrue(testCache.AddItem("test", value, 0), "Added valid data and was told it failed");
    }

    /// <summary>
    /// Test for adding an item using the global cache
    /// Items have no expiration.
    /// </summary>
    [Test]
    public void AddItemKeyValueTtlThreadedTest()
    {
      var testCache = new Cache();
      var addedinThread1 = false;
      var addedinThread2 = false;
      var thread1 = new Thread(() => addedinThread1 = testCache.AddItem("test1", "tester"));
      var thread2 = new Thread(() => addedinThread2 = testCache.AddItem("test1", "testing"));

      StartAndWaitForThreadsToFinish(thread1, thread2);

      var result2 = testCache.LookupValue("test1");
      Assert.IsNotNull(result2, "nothing was returned when test1 was looked up");

      // if a1 != a2 then that's fine
      // if they are the same then both adds worked but only one of the values should be there
      if ((addedinThread1 == addedinThread2) && addedinThread1)
      {
        Assert.IsNotNull(result2, "Nothing was saved but the return says something was");

        var first = string.CompareOrdinal(result2.ToString(), "tester");
        var second = string.CompareOrdinal(result2.ToString(), "testing");

        Assert.IsTrue(first == 0 || second == 0, "neither returns where what was expected");
      }           

      Assert.IsTrue(addedinThread1 == addedinThread2, "The results of the tread adding are not the same and should be"); 
    }

    /// <summary>
    /// Test for adding an item using the global cache
    /// Items have no expiration.
    /// </summary>
    [Test]
    public void AddItemKeyValueTtlThreadedZeroTimeTest()
    {
      var testCache = new Cache();
      var addedinThread1 = false;
      var addedinThread2 = false;

      // test that the add works when threaded and the first add's value has no time to live
      var thread1 = new Thread(() => addedinThread1 = testCache.AddItem("test1", "test3", 0));
      var thread2 = new Thread(() => addedinThread2 = testCache.AddItem("test1", "test4"));

      StartAndWaitForThreadsToFinish(thread1, thread2, 1000);

      var result = testCache.LookupValue("test1");
      Assert.IsNotNull(result, "nothing was returned when test1 was looked up");

      // TODO review this next bit
      Assert.IsTrue(addedinThread1 == addedinThread2, "The results of the tread adding are not the same and should be"); 
      Assert.IsTrue(string.CompareOrdinal(result.ToString(), "test4") == 0, string.Format("The expected value of [test4] was not returned got [{0}]", result));
    }

    /// <summary>
    /// Adds an item to the global cache.
    /// Items have no expiration.
    /// </summary>
    [Test]
    public void AddItemKeyValueTest()
    {
      var testCache = new Cache();
      var value = 1;

      Assert.IsTrue(testCache.AddItem("test", value), "Added valid data and was told it failed");

      // overwrite test
      Assert.IsTrue(testCache.AddItem("test", value), "Added valid data and was told it failed");
      var result1 = testCache.LookupValue("test");
      Assert.IsTrue(string.Compare(result1.ToString(), value.ToString(CultureInfo.InvariantCulture), StringComparison.Ordinal) == 0, string.Format("Comparison of [{0}] and [{1}] determined they where not the same and should have been", value, result1));

      value = 100;
      Assert.IsTrue(testCache.AddItem("test", value), "Added valid data and was told it failed");
      var result2 = testCache.LookupValue("test");
      Assert.IsTrue(string.CompareOrdinal(result2.ToString(), value.ToString(CultureInfo.InvariantCulture)) == 0, string.Format("Comparison of [{0}] and [{1}] determined they where not the same and should have been", value, result2));
      Assert.IsFalse(string.CompareOrdinal(result1.ToString(), result2.ToString()) == 0, string.Format("Comparison of [{0}] and [{1}] determined they where the same and should have been", result1, result2));
    }

    /// <summary>
    /// Adds an item to the global cache in 2 threads.
    /// Items have no expiration.
    /// </summary>
    [Test]
    public void AddItemKeyValueThreadedTest()
    {
      var testCache = new Cache();
      var addedByThread1 = false;
      var addedbyThread2 = false;
      var thread1 = new Thread(() => addedByThread1 = testCache.AddItem("test1", "tester"));
      var thread2 = new Thread(() => addedbyThread2 = testCache.AddItem("test1", "testing"));

      StartAndWaitForThreadsToFinish(thread1, thread2);

      var result = testCache.LookupValue("test1");
      Assert.IsNotNull(result, "nothing was returned when test1 was looked up");

      // if a1 != a2 then that's fine
      // if they are the same then both adds worked but only one of the values should be there
      if ((addedByThread1 == addedbyThread2) && addedByThread1)
      {
        Assert.IsNotNull(result, "Nothing was saved but the return says something was");

        var first = string.Compare(result.ToString(), "tester", StringComparison.Ordinal);
        var second = string.Compare(result.ToString(), "testing", StringComparison.Ordinal);

        Assert.IsTrue(first == 0 || second == 0, "neither returns where what was expected");
      } 
    }

    /// <summary>
    /// Adds a null item to the global cache.
    /// Items have no expiration.
    /// </summary>
    [Test]
    public void AddItemKeyValueNullTest()
    {
      const int Value = 1;
      var testCache = new Cache();

      Assert.IsFalse(testCache.AddItem(null, null), "Tried to add nulls as an item and was told it worked");
      Assert.IsFalse(testCache.AddItem(string.Empty, null), "Tried to add empty sting key as an item and was told it worked");
      Assert.IsFalse(testCache.AddItem(null, Value), "Tried to add nulls as a k and was told it worked");
      Assert.IsFalse(testCache.AddItem(string.Empty, Value), "Tried to add empty sting key as an item and was told it worked");
    }

    /// <summary>
    /// Test deletes the item from a given cache.
    /// </summary>
    [Test]
    public void DeleteItemKeyTypeTest()
    {
      const string Value1 = "test";
      const int Value2 = 101;
      var testCache = new Cache();
      var type1 = Value1.GetType();
      var type2 = Value2.GetType();

      Assert.IsFalse(testCache.DeleteItem("test", type1), "Delete returned true with nothing to delete");
      testCache.AddItem("test", Value1, type1);
      Assert.IsFalse(testCache.DeleteItem("test", type2), "Delete of the wrong type returned true and shouldn't have");
      testCache.AddItem("test", Value2, type2);
      Assert.IsTrue(testCache.DeleteItem("test", type2), "Delete of the correct type returned false and shouldn't have");
      Assert.IsNull(testCache.LookupValue("test", type2), "Lookup found something that should have been deleted");
      Assert.IsTrue(testCache.DeleteItem("test", type1), "Delete returned false with nothing something delete");
      Assert.IsNull(testCache.LookupValue("test", type1), "Lookup found something that should have been deleted");

      // TODO add multi threaded code
    }

    /// <summary>
    /// Test deletes an item from the global pool
    /// </summary>
    [Test]
    public void DeleteItemKeyTest()
    {
      const string TestValue = "test";
      var testCache = new Cache();

      Assert.IsFalse(testCache.DeleteItem(TestValue), "Delete returned true with nothing to delete");
      testCache.AddItem(TestValue, TestValue);
      Assert.IsTrue(testCache.DeleteItem(TestValue), "Delete returned false with nothing something delete");
      Assert.IsNull(testCache.LookupValue(TestValue), "Lookup found something that should have been deleted");

      // TODO add multi threaded code
    }

    /// <summary>
    /// Test Lookups the value of a key from a given cache.
    /// </summary>
    [Test]
    public void LookupValueKeyTypeNullTest()
    {
      const string Value1 = "test";
      const int Value2 = 404;
      var testCache = new Cache();
      var type1 = Value1.GetType();
      var type2 = Value2.GetType();

      Assert.IsNull(testCache.LookupValue(null, null), "Was expecting null but got a value");
      Assert.IsNull(testCache.LookupValue(null, type1), "Was expecting null but got a value");
      Assert.IsNull(testCache.LookupValue(null, type2), "Was expecting null but got a value");
      Assert.IsNull(testCache.LookupValue(string.Empty, type1), "Was expecting null but got a value");
      Assert.IsNull(testCache.LookupValue(string.Empty, type2), "Was expecting null but got a value");
    }

    /// <summary>
    /// Test Lookups the value of a key from a given cache.
    /// </summary>
    [Test]
    public void LookupValueKeyTypeTest()
    {
      const string Value1 = "test";
      const int Value2 = 404;
      var testCache = new Cache();
      var type1 = Value1.GetType();
      var type2 = Value2.GetType();

      testCache.AddItem("test1", Value1, type1);
      testCache.AddItem("test2", Value2, type2);

      Assert.IsNull(testCache.LookupValue("test1", type2), "Was expecting null but got a value");
      Assert.IsNull(testCache.LookupValue("test2", type1), "Was expecting null but got a value");

      var result1 = testCache.LookupValue("test1", type1);
      var result2 = testCache.LookupValue("test2", type2);

      Assert.IsNotNull(result1, "Was expecting a value but got null for r1");
      Assert.IsNotNull(result2, "Was expecting a value but got null for r2");

      Assert.IsTrue(AreTypesTheSame(type1, result1), string.Format("The type returned for r1 [{0}] was not the same as t1 [{1}]", result1.GetType().Name, type1.Name));
      Assert.IsTrue(AreTypesTheSame(type2, result2), string.Format("The type returned for r2 [{0}] was not the same as t2 [{1}]", result2.GetType().Name, type2.Name));

      Assert.IsTrue(string.CompareOrdinal(Value1, result1.ToString()) == 0, string.Format("r1 [{0}] is not the same as v1 [{1}]", result1, Value1));
      Assert.IsTrue(string.CompareOrdinal(Value2.ToString(CultureInfo.InvariantCulture), result2.ToString()) == 0, string.Format("r2 [{0}] is not the same as v2 [{1}]", result2, Value2));
    }

    /// <summary>
    /// Test lookups the value of a key from the global cache.
    /// </summary>
    [Test]
    public void LookupValueKeyTest()
    {
      const string Key = "test";
      var testCache = new Cache();
      var value = 1;

      Assert.IsNull(testCache.LookupValue(null), "Was expecting null but got a value");
      Assert.IsNull(testCache.LookupValue(string.Empty), "Was expecting null but got a value");

      testCache.AddItem(Key, value);
      var r = testCache.LookupValue(Key);

      Assert.IsTrue(string.CompareOrdinal(value.ToString(CultureInfo.InvariantCulture), r.ToString()) == 0, string.Format("was expecting [{0}] but got [{1}]", value, r));

      var r2 = testCache.LookupValue(Key + "1");
      Assert.IsNull(r2, "was expecting null but got a value");

      value++;
      r = testCache.LookupValue(Key);

      Assert.IsFalse(string.CompareOrdinal(value.ToString(CultureInfo.InvariantCulture), r.ToString()) == 0, string.Format("was not expecting [{0}] but got [{1}]", value, r));
    }

    /// <summary>
    /// Lookups the timed entry expired test.
    /// </summary>
    [Test]
    public void LookupTimedEntryExpiredTest()
    {
      Assert.Fail("write tests to cover looking up data that has an expired time");
    }

    /// <summary>
    /// Dispose tests.
    /// </summary>
    [Test]
    public void DisposeTest()
    {
      var testCache = new Cache();
      testCache.AddItem("test", 1);
      testCache.Dispose();
      var excpetionThrown = false;

      try
      {
        testCache.LookupValue("test");
        Assert.Fail("an exception should have been thrown but wasn't");
      }
      catch (NullReferenceException)
      {
        excpetionThrown = true;
      }
      catch (Exception ex)
      {
        Assert.Fail("an unexpected exception was thrown {0}", ex.Message);
      }

      Assert.IsTrue(excpetionThrown, "No exception thrown when looking up data from a disposed object and it should have been");
    }

    /// <summary>
    /// Are the types the same.
    /// </summary>
    /// <param name="one">the first object to check.</param>
    /// <param name="two">the second object to check.</param>
    /// <returns><c>true</c>, if types are the same, <c>false</c> otherwise.</returns>
    private static bool AreTypesTheSame(object one, object two)
    {
      var oneType = one.GetType();
      return oneType.Equals(two);
    }

    /// <summary>
    /// Starts the threads and wait for them to finish.
    /// </summary>
    /// <param name="thread1">First thread to start.</param>
    /// <param name="thread2">Second thread to start.</param>
    /// <param name="sleepTime">number of milliseconds to wait between starting threads</param>
    private static void StartAndWaitForThreadsToFinish(Thread thread1, Thread thread2, int sleepTime = 0)
    {
      thread1.Start();
      Thread.Sleep(sleepTime);     // need to do this to give at least 1 second between the first add and the second so it can timeout properly
      thread2.Start();

      // wait for the threads to finish
      while (thread1.IsAlive && thread2.IsAlive)
      {
      }
    }
  }
}