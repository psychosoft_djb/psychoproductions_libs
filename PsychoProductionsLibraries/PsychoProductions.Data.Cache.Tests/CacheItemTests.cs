//-----------------------------------------------------------------------
// <copyright file="CacheItemTests.cs" company="Psycho Productions">
//      <author>David Buckley</author>
//      <email>david@psychosoft.co.uk</email>
// </copyright>
//-----------------------------------------------------------------------
//
// Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2013, Psycho Productions
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace PsychoProductions.Data.Cache.Tests
{
    using System;

    using NUnit.Framework;

    using PsychoProductions.Data.Cache;

    /// <summary>
    /// Cache item tests.
    /// </summary>
    [TestFixture]
    public class CacheItemTests
    {
        /// <summary>
        /// Cache item constructor tests
        /// </summary>
        [Test]
        public void CacheItemConstructorEmptyKeyTest()
        {
            try
            {
                Assert.IsNull(new CacheItem(string.Empty, null, 0));
            }
            catch (ArgumentNullException)
            {
            }
                // ReSharper disable once CatchAllClause
            catch (Exception e)
            {
                Assert.Fail("Unknown exception thrown [{0}]", e);
            }
        }

        /// <summary>
        /// Cache item constructor tests with Nulls
        /// </summary>
        [Test]
        public void CacheItemConstructorNullTest()
        {
            try
            {
                Assert.IsNull(new CacheItem(null, null, 0));
            }
            catch (ArgumentNullException)
            {
            }
                // ReSharper disable once CatchAllClause
            catch (Exception e)
            {
                Assert.Fail("Unknown exception thrown [{0}]", e);
            }
        }

        /// <summary>
        /// Cache item constructor tests
        /// </summary>
        [Test]
        public void CacheItemConstructorTest()
        {
            Assert.IsNotNull(new CacheItem("test", null, 0), "The object is null and shouldn't be");
        }

        /// <summary>
        /// Dispose tests.
        /// </summary>
        [Test]
        public void DisposeTest()
        {
            var testItem = new CacheItem("test", null, -1);
            testItem.Dispose();
            Assert.IsNull(testItem.Key, "the key was not null [{0}]", testItem.Key);
            Assert.IsNull(testItem.Value, "the item was not null");
        }

        /// <summary>
        /// Determines whether this instance is expired.
        /// </summary>
        [Test]
        public void IsExpiredPropertyTest()
        {
            var o = new CacheItem("test", null, -1);
            Assert.IsFalse(o.IsExpired, "object has expired with infinite time");

            o = new CacheItem("test", null, 0);
            System.Threading.Thread.Sleep(Milliseconds(1));                // the test could come in the same second as this was set so it will never expire
            Assert.IsTrue(o.IsExpired, "object has not expired with 0 time");

            o = new CacheItem("test", null, -1);
            Assert.IsFalse(o.IsExpired, "object has expired with -1 time");

            o = new CacheItem("test", null, 10);
            Assert.IsFalse(o.IsExpired, "object has expired with 10 seconds");
            System.Threading.Thread.Sleep(Milliseconds(11));               // make sure it expires
            Assert.IsTrue(o.IsExpired, "object has not expired with 10 seconds delay");
        }

        /// <summary>
        /// Tests for the cache item Key
        /// </summary>
        [Test]
        public void KeyPropertyTest()
        {
            var o = new CacheItem("test", null, 0);
            Assert.IsTrue(string.Compare(o.Key, "test", StringComparison.CurrentCulture) == 0, "The key returned was not [test] but [" + o.Key + "]");
        }

        /// <summary>
        /// Cache item constructor tests
        /// </summary>
        [Test]
        public void MakeCacheItemEmptyKeyTest()
        {
            try
            {
                Assert.IsNull(CacheItem.MakeCacheItem(string.Empty, null, 0));
            }
            catch (ArgumentNullException)
            {
            }
                // ReSharper disable once CatchAllClause
            catch (Exception e)
            {
                Assert.Fail("Unknown exception thrown [{0}]", e);
            }
        }

        /// <summary>
        /// Cache item constructor tests with Nulls
        /// </summary>
        [Test]
        public void MakeCacheItemNullTest()
        {
            try
            {
                Assert.IsNull(CacheItem.MakeCacheItem(null, null, 0));
            }
            catch (ArgumentNullException)
            {
            }
                // ReSharper disable once CatchAllClause
            catch (Exception e)
            {
                Assert.Fail("Unknown exception thrown [{0}]", e);
            }
        }

        /// <summary>
        /// Cache item constructor tests
        /// </summary>
        [Test]
        public void MakeCacheItemTest()
        {
            Assert.IsNotNull(CacheItem.MakeCacheItem("test", null, 0), "The object is null and shouldn't be");
        }

        /// <summary>
        /// tests for the cache item value
        /// </summary>
        [Test]
        public void ValuePropertyTest()
        {
            var o = new CacheItem("test", null, 0);
            Type t;

            Assert.IsNull(o.Value, "The object value should have been null but is not");

            o = new CacheItem("test", "test", 0);
            t = o.Value.GetType();
            Assert.IsTrue(t == typeof(string), "The object type returned is not a [string] but [" + t.Name + "]");
            Assert.IsTrue(string.Compare(o.Value.ToString(), "test", StringComparison.CurrentCulture) == 0, string.Format("The key returned was not [test] but [{0}]", o.Value));

            o = new CacheItem("test", 1, 0);
            t = o.Value.GetType();
            Assert.IsTrue(t == typeof(int), "The object type returned is not a [int] but [" + t.Name + "]");
            Assert.IsTrue((int)o.Value == 1, string.Format("The key returned was not [1] but [{0}]", o.Value));
        }

        /// <summary>
        /// Get the number of seconds in milliseconds
        /// </summary>
        /// <param name="seconds">the number of seconds to convert</param>
        /// <returns>the number of milliseconds in seconds</returns>
        private static int Milliseconds(int seconds)
        {
            return seconds * 1000;
        }
    }
}